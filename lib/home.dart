import 'package:flutter/material.dart';

class MyHome extends StatelessWidget {
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xF7FBFF),
      resizeToAvoidBottomInset: false,
      bottomNavigationBar: getFooter(),
    );
  }

  Widget getFooter(){
    return Container (
      height: 80,
      decoration: BoxDecoration(
        color: Colors.white
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Icon(
            Icons.arrow_back_ios,
            color: Color(0x73090F47),
            size: 40.0,
          ),
          GestureDetector(
            onTap: () {}, // Image tapped
            child: Image.asset(
              'assets/logo.png',
              fit: BoxFit.cover, // Fixes border issues
              width: 40.0,
              height: 40.0,
            ),
          ),
        ],
      ),
      
    );
  }
}
