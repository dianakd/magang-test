import 'package:app/Awal/auth/verify/verifycode.dart';
import 'package:app/home.dart';
import 'package:flutter/material.dart';
import 'Awal/splash/splashscreen.dart';
import 'Awal/auth/login.dart';
import 'Awal/auth/signup.dart';
import 'Awal/auth/verify/verifycode.dart';
import 'Awal/auth/verify/verified.dart';
import 'package:app/homescreen.dart';

void main() {
  runApp(
    MaterialApp(
      title: 'Named Routes Demo',
      // Start the app with the "/" named route. In this case, the app starts
      // on the FirstScreen widget.
      initialRoute: '/',
      routes: {
        // When navigating to the "/" route, build the FirstScreen widget.
        '/': (context) => const SplaashScreen(),
        '/second': (context) => const SecondScreen(),
        '/login':(context) => const Login(),
        '/signup':(context) => const SignUp(),
        '/verify':(context) => const VerifyCode(),
        '/verified':(context) => const Verified(),
        '/home':(context) => MyHome(),
        '/homescreen':(context) => HomeScreen(),
      },

      debugShowCheckedModeBanner: false,
    ),
  );
}



class SecondScreen extends StatelessWidget {
  const SecondScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Second Screen'),
      ),
      body: Center(
        child: ElevatedButton(
          // Within the SecondScreen widget
          onPressed: () {
            // Navigate back to the first screen by popping the current route
            // off the stack.
            Navigator.pop(context);
          },
          child: const Text('Go back!'),
        ),
      ),
    );
  }
}
