// import 'package:flutter/material.dart';
// import 'package:intro_screen_onboarding_flutter/intro_app.dart';
// import 'home.dart';


// class SecondScreen extends StatelessWidget {
//   const SecondScreen({super.key});

//   Widget build(BuildContext context) {
//     return MaterialApp(
//       title: 'Flutter Demo',
//       theme: ThemeData(
//         primarySwatch: Colors.blue,
//         // backgroundColor: Color.fromARGB(255, 36, 34, 34),
//         visualDensity: VisualDensity.adaptivePlatformDensity,
//       ),
//       home: TestScreen(),
//     );
//   }
// }

// class TestScreen extends StatelessWidget {
//   final List<Introduction> list = [
//     Introduction(
//       title: 'Buy & Sell',
//       subTitle: 'Browse the menu and order directly from the application',
//       imageUrl: 'assets/splash1.png',
//     ),
//     Introduction(
//       title: 'Delivery',
//       subTitle: 'Your order will be immediately collected and',
//       imageUrl: 'assets/splash1.png',
//     ),
//     Introduction(
//       title: 'Receive Money',
//       subTitle: 'Pick up delivery at your door and enjoy groceries',
//       imageUrl: 'assets/splash1.png',
//     ),
//     Introduction(
//       title: 'Finish',
//       subTitle: 'Browse the menu and order directly from the application',
//       imageUrl: 'assets/splash1.png',
//     ),
//   ];

//   @override
//   Widget build(BuildContext context) {
//     return IntroScreenOnboarding(
//       introductionList: list,
//       onTapSkipButton: () {
//         Navigator.push(
//           context,
//           MaterialPageRoute(
//             builder: (context) => MyHome(),
//           ), //MaterialPageRoute
//         );
//       },
//       // foregroundColor: Colors.red,
//     );
//   }
// }