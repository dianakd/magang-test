
import 'package:flutter/material.dart';
import 'package:flutter_onboarding_slider/flutter_onboarding_slider.dart';
import 'package:google_fonts/google_fonts.dart';


class SplaashScreen extends StatefulWidget {
  const SplaashScreen({super.key});
  
  @override
  State<SplaashScreen> createState() => _SplaashScreenState();
}

class _SplaashScreenState extends State<SplaashScreen> {
  final Color kDarkBlueColor = Color(0xFF053149);

  @override
  Widget build(BuildContext context) { 
    
    return Scaffold(
      backgroundColor: Colors.white,
      body: Center(
        child: SizedBox(
          height: MediaQuery.of(context).size.height * 0.95, //height to 10% of screen height, 100/10 = 0.1
          width: MediaQuery.of(context).size.width * 0.99,
          child: OnBoardingSlider(
          finishButtonText: 'Back',
          onFinish: () {
            Navigator.pushNamed(context, '/');
          },
          finishButtonColor: Color(0xFF4157FF),
          skipTextButton: Text(
            'Skip',
            style: GoogleFonts.overpass(
              fontSize: 16,
              color: kDarkBlueColor,
              fontWeight: FontWeight.w600,
            ),
          ),
          controllerColor: Color(0xFFF090F47),
          totalPage: 4,
          headerBackgroundColor: Colors.white,
          pageBackgroundColor: Colors.white,
          background: [
            Image.asset(
              'assets/splash1.png',
              height: 370,
              width: 580,
              
              alignment: Alignment.bottomCenter,
            ),
            Image.asset(
              'assets/splash2.png',
              height: 360,
              width: 600,
              fit: BoxFit.contain,
              alignment: Alignment.bottomCenter,
            ),
            Image.asset(
              'assets/splash3.png',
              height: 420,
              width: 570,
              alignment: Alignment.center,
            ),
            Image.asset(
              'assets/splash4.png',
              height: 590,
              alignment: Alignment.center,
            ),
          ],
          speed: 1.8,
          pageBodies: [
            Container(
              height: MediaQuery.of(context).size.height * 0.05, //height to 10% of screen height, 100/10 = 0.1
              width: MediaQuery.of(context).size.width * 0.6, 
              padding: EdgeInsets.symmetric(horizontal: 40),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  SizedBox(
                    height: 480,
                  ),
                  Text(
                    'View and buy Medicine online',
                    textAlign: TextAlign.center,
                    style: GoogleFonts.overpass(
                      color: kDarkBlueColor,
                      fontSize: 24.0,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Container(
                    child: Text(
                      'Etiam mollis metus non purus faucibus sollicitudin. Pellentesque sagittis mi. Integer.',
                      textAlign: TextAlign.center,
                      style: GoogleFonts.overpass(
                        color: Colors.black26,
                        fontSize: 16.0,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                    width: MediaQuery.of(context).size.width * 0.45,
                  ),
                ],
              ),
            ),
            Container(
              height: MediaQuery.of(context).size.height * 0.05, //height to 10% of screen height, 100/10 = 0.1
              width: MediaQuery.of(context).size.width * 0.9, 
              padding: EdgeInsets.symmetric(horizontal: 40),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  SizedBox(
                    height: 480,
                  ),
                  Text(
                    'Online medical % Healthcare',
                    textAlign: TextAlign.center,
                    style: GoogleFonts.overpass(
                      color: kDarkBlueColor,
                      fontSize: 24.0,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Container(
                    child: Text(
                      'Do you want some help with your health to get better life?',
                      textAlign: TextAlign.center,
                      style: GoogleFonts.overpass(
                        color: Colors.black26,
                        fontSize: 16.0,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                    width: MediaQuery.of(context).size.width * 0.45,
                  ),
                ],
              ),
            ),
            Container(
              height: MediaQuery.of(context).size.height * 0.05, //height to 10% of screen height, 100/10 = 0.1
              width: MediaQuery.of(context).size.width * 9, 
              padding: EdgeInsets.symmetric(horizontal: 40),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  SizedBox(
                    height: 480,
                  ),
                  Text(
                    'Get Delivery on time',
                    textAlign: TextAlign.center,
                    style: GoogleFonts.overpass(
                      color: kDarkBlueColor,
                      fontSize: 24.0,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Container(
                    child: Text(
                      'Etiam mollis metus non purus faucibus sollicitudin. Pellentesque sagittis mi. Integer.',
                      textAlign: TextAlign.center,
                      style: GoogleFonts.overpass(
                        color: Colors.black26,
                        fontSize: 16.0,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                    width: MediaQuery.of(context).size.width * 0.45,
                  ),
                ],
              ),
            ),
            Container(
              height: MediaQuery.of(context).size.height * 1, //height to 10% of screen height, 100/10 = 0.1
              width: MediaQuery.of(context).size.width * 0.9, 
              padding: EdgeInsets.symmetric(horizontal: 40),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  SizedBox(
                    height: 480,
                  ),
                  Text(
                    'Welcome to Medtech',
                    textAlign: TextAlign.center,
                    style: GoogleFonts.overpass(
                      color: kDarkBlueColor,
                      fontSize: 24.0,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Container(
                    child: Text(
                      'Do you want some help with your health to get better life?',
                      textAlign: TextAlign.center,
                      style: GoogleFonts.overpass(
                        color: Colors.black26,
                        fontSize: 16.0,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                    width: MediaQuery.of(context).size.width * 0.45,
                  ),
                  
                  SizedBox(
                    height: 30,
                  ),
                  
                  SizedBox(
                    height: MediaQuery.of(context).size.height * 0.05, //height to 10% of screen height, 100/10 = 0.1
                    width: MediaQuery.of(context).size.width * 0.6, 
                    child: ElevatedButton(
                    onPressed: () {
                      Navigator.pushNamed(context, '/login'); 
                    },
                    style: ElevatedButton.styleFrom(
                        shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20.0)),
                        primary: Color(0xFF4157FF),
                      ),
                      
                      child: Text(
                        "SIGN UP WITH EMAIL",
                        style: GoogleFonts.overpass(
                          color: Colors.white, 
                          fontSize: 16,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                  ),
                  
                  

                  SizedBox(
                    height: 10,
                  ),
                  Container(
                      height: MediaQuery.of(context).size.height * 0.05, //height to 10% of screen height, 100/10 = 0.1
                      width: MediaQuery.of(context).size.width * 0.6, //width t 70% of screen width
                      padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                      child: OutlinedButton.icon(
                        onPressed: () {
                          Navigator.pushNamed(context, '/login'); 
                        },
                        icon: Image.asset('assets/fb.png' ,width: 18,height: 18,  ),
                        style: OutlinedButton.styleFrom(
                          padding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
                          side: const BorderSide(
                            color: Color(0x1A090F47),
                          ),
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(20),
                          ),
                        ),
                        label: Text('CONTINUE WITH FACEBOOK',
                          style: GoogleFonts.overpass(
                                fontSize: 13.0,
                                color: Color(0xFF090F47),
                                height: 1.5,
                                fontWeight: FontWeight.bold,
                              ),
                              textAlign: TextAlign.center
                          ),
                      )
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Container(
                      height: MediaQuery.of(context).size.height * 0.05, //height to 10% of screen height, 100/10 = 0.1
                      width: MediaQuery.of(context).size.width * 0.6, //width t 70% of screen width
                      padding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
                      child: OutlinedButton.icon(
                        onPressed: () {
                          Navigator.pushNamed(context, '/login'); 
                        },
                        icon: Image.asset('assets/google.png' ,width: 18,height: 18,  ),
                        style: OutlinedButton.styleFrom(
                          padding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
                          side: const BorderSide(
                            color: Color(0x1A090F47),
                          ),
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(20),
                          ),
                        ),
                        label: Text('CONTINUE WITH FACEBOOK',
                          style: GoogleFonts.overpass(
                                fontSize: 13.0,
                                color: Color(0xFF090F47),
                                height: 1.5,
                                fontWeight: FontWeight.bold,
                              ),
                              textAlign: TextAlign.center
                          ),
                      )
                    ),

                ],
              ),
            ),
          ],
        ),
        ),
      ),
    );
    
  }
}