import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class Login extends StatelessWidget {
  const Login({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Center(
        child: SizedBox(
          height: MediaQuery.of(context).size.height * 0.9, //height to 10% of screen height, 100/10 = 0.1
          width: MediaQuery.of(context).size.width * 0.8,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Align(
                alignment: Alignment.topLeft,
                child: IconButton(
                  icon: Image.asset('assets/back.png' ,width: 35, height: 35, color: Color(0xFF090F47),),  
                  alignment: Alignment.topLeft,
                  onPressed: () {
                    Navigator.pop(context);
                  },
                ),
              ),
              SizedBox(
                  height: 20,
              ),
              Padding(
                padding: EdgeInsets.fromLTRB(10, 0, 0, 0),
                child: Align(
                  alignment: Alignment.centerLeft,
                  child:  Text(
                    'Welcome Back!',
                    textAlign: TextAlign.left,
                    style: GoogleFonts.overpass(
                      fontSize: 26,
                      fontWeight: FontWeight.w700,
                    ),
                  ),
                ),
              ),
             
              SizedBox(
                  height: 30,
                ),
              TextField(
                decoration: InputDecoration(
                  focusedBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: Color(0x73090F47),),
                  ),
                  enabledBorder: UnderlineInputBorder(
                    borderSide: BorderSide(
                      color: Color(0x1A090F47),
                    ),
                  ),
                  prefixIcon: Image.asset('assets/user.png' ,width: 25,height: 25, color: Color(0xFF090F47),  ),
                  hintText: "Username",
                  hintStyle: TextStyle(color: Color(0x73090F47), fontSize: 20.0,),
                ),
              ),
              SizedBox(
                  height: 10,
                ),
              TextField(
                decoration: InputDecoration(
                  focusedBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: Color(0x73090F47),),
                  ),
                  enabledBorder: UnderlineInputBorder(
                    borderSide: BorderSide(
                      color: Color(0x1A090F47),
                    ),
                  ),
                  prefixIcon: Image.asset('assets/lock.png' ,width: 25,height: 25, color: Color(0xFF090F47),  ),
                  hintText: "Password",
                  hintStyle: TextStyle(color: Color(0x73090F47), fontSize: 20.0,),
                ),
              ),
              SizedBox(
                  height: 30,
                ),
              SizedBox(
                height: 60, //height to 10% of screen height, 100/10 = 0.1
                width: MediaQuery.of(context).size.width * 0.9, 
                child: ElevatedButton(
                onPressed: () {
                  Navigator.pushNamed(context, '/homescreen'); 
                },
                style: ElevatedButton.styleFrom(
                    shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(56.0)),
                    primary: Color(0xFF4157FF),
                  ),
                  
                  child: Text(
                    "LOGIN",
                    style: GoogleFonts.overpass(
                      color: Colors.white, 
                      fontSize: 18,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              ),
              SizedBox(
                  height: 30,
              ),
              new GestureDetector(
                onTap: () {
                  Navigator.pushNamed(context, '/signup');
                },
                child: Align(
                  alignment: Alignment.bottomCenter,
                  child:Container(
                    width: MediaQuery.of(context).size.width * 0.9, 
                    color: Colors.white,
                    child:Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Icon(
                          Icons.arrow_back_ios,
                          color: Color(0x73090F47),
                          size: 16.0,
                        ),
                        Text('Don’t have an account? Sign Up', 
                          style: GoogleFonts.poppins(
                          fontSize: 16,
                          fontWeight: FontWeight.w300,
                          color: Color(0xff090F47),
                        ),
                      ),
                        
                      ],
                    ),
                    
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}