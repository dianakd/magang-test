import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:flutter/services.dart';

class SignUp extends StatelessWidget {
  const SignUp({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Center(
        child: SizedBox(
          height: MediaQuery.of(context).size.height * 0.9, //height to 10% of screen height, 100/10 = 0.1
          width: MediaQuery.of(context).size.width * 0.8,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Align(
                alignment: Alignment.topLeft,
                child: IconButton(
                  icon: Image.asset('assets/back.png' ,width: 35, height: 35, color: Color(0xFF090F47),),  
                  alignment: Alignment.topLeft,
                  onPressed: () {
                    Navigator.pop(context);
                  },
                ),
              ),
              SizedBox(
                  height: 20,
              ),
              Padding(
                padding: EdgeInsets.fromLTRB(10, 0, 0, 0),
                child: Align(
                  alignment: Alignment.centerLeft,
                  child:  Text(
                    'Create your account',
                    textAlign: TextAlign.left,
                    style: GoogleFonts.overpass(
                      fontSize: 26,
                      fontWeight: FontWeight.w700,
                    ),
                  ),
                ),
              ),
             
              SizedBox(
                  height: 30,
                ),
              
              TextField(
                decoration: new InputDecoration(
                  focusedBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: Color(0x73090F47),),
                  ),
                  enabledBorder: UnderlineInputBorder(
                    borderSide: BorderSide(
                      color: Color(0x33090F47),
                    ),
                  ),
                  labelText: "Your Name", 
                  labelStyle: TextStyle(
                      color: Color(0x73090F47), 
                      fontSize: 20.0,
                  ),
                  hintStyle: TextStyle(color: Color(0x73090F47), fontSize: 20.0,),
                ),// Only numbers can be entered
              ),
              SizedBox(
                  height: 10,
                ),
              TextField(
                decoration: new InputDecoration(
                  focusedBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: Color(0x73090F47),),
                  ),
                  enabledBorder: UnderlineInputBorder(
                    borderSide: BorderSide(
                      color: Color(0x33090F47),
                    ),
                  ),
                  labelText: "Mobile Number", 
                  labelStyle: TextStyle(
                      color: Color(0x73090F47), 
                      fontSize: 20.0,
                  ),
                ),
                keyboardType: TextInputType.number,
                inputFormatters: <TextInputFormatter>[
                      FilteringTextInputFormatter.digitsOnly
                ], // Only numbers can be entered
              ),
              SizedBox(
                  height: 10,
                ),
              TextField(
                decoration: new InputDecoration(
                  focusedBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: Color(0x73090F47),),
                  ),
                  enabledBorder: UnderlineInputBorder(
                    borderSide: BorderSide(
                      color: Color(0x33090F47),
                    ),
                  ),
                  labelText: "Email", 
                  labelStyle: TextStyle(
                      color: Color(0x73090F47), 
                      fontSize: 20.0,
                  ),
                ),// Only numbers can be entered
              ),
              SizedBox(
                  height: 10,
                ),
              TextField(
                decoration: new InputDecoration(
                  focusedBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: Color(0x73090F47),),
                  ),
                  enabledBorder: UnderlineInputBorder(
                    borderSide: BorderSide(
                      color: Color(0x33090F47),
                    ),
                  ),
                  labelText: "Password", 
                  labelStyle: TextStyle(
                      color: Color(0x73090F47), 
                      fontSize: 20.0,
                  ),
                ),// Only numbers can be entered
              ),
              SizedBox(
                  height: 30,
              ),
              SizedBox(
                height: 60, //height to 10% of screen height, 100/10 = 0.1
                width: MediaQuery.of(context).size.width * 0.9, 
                child: ElevatedButton(
                onPressed: () {
                  Navigator.pushNamed(context, '/verify'); 
                },
                style: ElevatedButton.styleFrom(
                    shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(56.0)),
                    primary: Color(0xFF4157FF),
                  ),
                  
                  child: Text(
                    "CREATE ACCOUNT",
                    style: GoogleFonts.overpass(
                      color: Colors.white, 
                      fontSize: 18,
                    ),
                  ),
                ),
              ),
              SizedBox(
                  height: 30,
              ),
              new GestureDetector(
                onTap: () {
                  Navigator.pop(context);
                },
                child: Align(
                  alignment: Alignment.bottomCenter,
                  child:Container(
                    width: MediaQuery.of(context).size.width * 0.9, 
                    color: Colors.white,
                    child:Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Icon(
                          Icons.arrow_back_ios,
                          color: Color(0x73090F47),
                          size: 16.0,
                        ),
                        Text('Already have account?  Login', 
                          style: GoogleFonts.poppins(
                          fontSize: 16,
                          fontWeight: FontWeight.w300,
                          color: Color(0xff090F47),
                        ),
                      ),
                        
                      ],
                    ),
                    
                  ),
                ),
              ),
              
            ],
          ),
        ),
      ),
    );
  }
}