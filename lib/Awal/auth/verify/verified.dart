import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class Verified extends StatelessWidget {
  const Verified({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Center(
        child: SizedBox(
          height: MediaQuery.of(context).size.height * 0.9, //height to 10% of screen height, 100/10 = 0.1
          width: MediaQuery.of(context).size.width * 0.8,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Align(
                alignment: Alignment.topLeft,
                child: IconButton(
                  icon: Image.asset('assets/back.png' ,width: 35, height: 35, color: Color(0xFF090F47),),  
                  alignment: Alignment.topLeft,
                  onPressed: () {
                    Navigator.pop(context);
                  },
                ),
              ),
              SizedBox(
                  height: MediaQuery.of(context).size.height * 0.1,
              ),
              Container(
              height: MediaQuery.of(context).size.height * 0.3, //height to 10% of screen height, 100/10 = 0.1
              width: MediaQuery.of(context).size.width * 0.8,
              decoration: new BoxDecoration(
              image: new DecorationImage(
                  image: ExactAssetImage('assets/verif.png'),
                  fit: BoxFit.fitHeight,
                  ),
              )),
              SizedBox(
                  height: 40,
              ),
              Padding(
                padding: EdgeInsets.fromLTRB(10, 0, 0, 0),
                child: Align(
                  alignment: Alignment.center,
                  child:  Text(
                    'Phone Number Verified',
                    textAlign: TextAlign.left,
                    style: GoogleFonts.overpass(
                      fontSize: 26,
                      fontWeight: FontWeight.w700,
                    ),
                  ),
                ),
              ),
              SizedBox(
                  height: 20,
              ),
              Padding(
                padding: EdgeInsets.fromLTRB(10, 0, 0, 0),
                child: Align(
                  alignment: Alignment.center,
                  child:  Text(
                    'Congradulations, your phone number has been verified. You can start using the app',
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                      fontSize: 18,
                      fontWeight: FontWeight.w300,
                      color: Color(0x73090F47),
                    ),
                  ),
                ),
              ),
              SizedBox(
                  height: 180,
              ),
             
              SizedBox(
                height: 60, //height to 10% of screen height, 100/10 = 0.1
                width: MediaQuery.of(context).size.width * 0.9, 
                child: ElevatedButton(
                onPressed: () {
                  Navigator.pushNamed(context, '/homescreen'); 
                },
                style: ElevatedButton.styleFrom(
                    shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(56.0)),
                    primary: Color(0xFF4157FF),
                  ),
                  
                  child: Text(
                    "CONTINUE",
                    style: GoogleFonts.overpass(
                      color: Colors.white, 
                      fontSize: 18,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              ),

                      
            ],
          ),
        ),
      ),
    );
  }
}