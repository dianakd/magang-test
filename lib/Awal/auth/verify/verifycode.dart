import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:otp_text_field/otp_field.dart';
import 'package:otp_text_field/style.dart';

class VerifyCode extends StatelessWidget {
  const VerifyCode({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Center(
        child: SizedBox(
          height: MediaQuery.of(context).size.height * 0.9, //height to 10% of screen height, 100/10 = 0.1
          width: MediaQuery.of(context).size.width * 0.8,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Align(
                alignment: Alignment.topLeft,
                child: IconButton(
                  icon: Image.asset('assets/back.png' ,width: 35, height: 35, color: Color(0xFF090F47),),  
                  alignment: Alignment.topLeft,
                  onPressed: () {
                    Navigator.pop(context);
                  },
                ),
              ),
              SizedBox(
                  height: 30,
              ),
              Padding(
                padding: EdgeInsets.fromLTRB(10, 0, 0, 0),
                child: Align(
                  alignment: Alignment.centerLeft,
                  child:  Text(
                    'Enter the verify code',
                    textAlign: TextAlign.left,
                    style: GoogleFonts.overpass(
                      fontSize: 26,
                      fontWeight: FontWeight.w700,
                    ),
                  ),
                ),
              ),
              SizedBox(
                  height: 20,
              ),
              Padding(
                padding: EdgeInsets.fromLTRB(10, 0, 0, 0),
                child: Align(
                  alignment: Alignment.centerLeft,
                  child:  Text(
                    'We just send you a verification code via phone +65 556 798 241',
                    textAlign: TextAlign.left,
                    style: GoogleFonts.poppins(
                      fontSize: 16,
                      fontWeight: FontWeight.w300,
                      color: Color(0x73090F47),
                    ),
                  ),
                ),
              ),
              SizedBox(
                  height: 30,
                ),
              OTPTextField(
                length: 5,
                width: MediaQuery.of(context).size.width,
                fieldWidth: 80,
                style: TextStyle(
                  fontSize: 17
                ),
                textFieldAlignment: MainAxisAlignment.spaceAround,
                fieldStyle: FieldStyle.underline,
                onCompleted: (pin) {
                  print("Completed: " + pin);
                },
              ),
              SizedBox(
                  height: 30,
                ),
              SizedBox(
                height: 60, //height to 10% of screen height, 100/10 = 0.1
                width: MediaQuery.of(context).size.width * 0.9, 
                child: ElevatedButton(
                onPressed: () {
                  Navigator.pushNamed(context, '/verified'); 
                },
                style: ElevatedButton.styleFrom(
                    shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(56.0)),
                    primary: Color(0xFF4157FF),
                  ),
                  
                  child: Text(
                    "SUBMIT CODE",
                    style: GoogleFonts.overpass(
                      color: Colors.white, 
                      fontSize: 18,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              ),
              SizedBox(
                  height: 30,
              ),
              Container(
                width: MediaQuery.of(context).size.width * 0.9, 
                color: Colors.white,
                child: Align(
                  alignment: Alignment.center,
                  child: Text('The verify code will expire in 00:59', 
                    style: GoogleFonts.poppins(
                      fontSize: 16,
                      fontWeight: FontWeight.w300,
                      color: Color(0xff090F47),
                    ),
                    textAlign: TextAlign.center,
                  ),
                ),
              ),
              SizedBox(
                  height: 20,
              ),
              new GestureDetector(
                onTap: () {
                  Navigator.pushNamed(context, '/');
                },
                child:Container(
                    width: MediaQuery.of(context).size.width * 0.9, 
                    color: Colors.white,
                    child: Align(
                      alignment: Alignment.center,
                      child: Text('Resend Code', 
                      style: GoogleFonts.poppins(
                        fontSize: 16,
                        fontWeight: FontWeight.w300,
                        color: Color(0xFF4157FF),
                      ),
                      textAlign: TextAlign.center,
                    ),
                      
                    ),
                    
                  ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}