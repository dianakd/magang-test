import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:app/model/model.dart';
import 'package:app/assets.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFFF7FBFF),
      body: Stack(
        children: <Widget> [
          background(),
          SafeArea(
            child: SingleChildScrollView(
              child: Column(

                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      
                      Container(
                        margin: EdgeInsets.fromLTRB(20, 30, 0, 20),
                        child: Align(
                          alignment: Alignment.topLeft,
                          child: IconButton(
                            icon: Image.asset('assets/profile.png' ,width: 50, height: 50, ),  
                            alignment: Alignment.topLeft,
                            onPressed: () {
                              Navigator.pop(context);
                            },
                          ),
                        ),
                      ),
                      Expanded(
                        child: Container(
                          alignment: Alignment.center,
                          width: MediaQuery.of(context).size.width * 0.6,
                        ),
                      ),
                      Container(
                        child: Align(
                          alignment: Alignment.centerRight,
                          child: IconButton(
                            icon: Image.asset('assets/notif.png' ,width: 45, height: 45, ),  
                            alignment: Alignment.topLeft,
                            onPressed: () {
                              Navigator.pop(context);
                            },
                          ),
                        ),
                      ),
                      SizedBox(
                        width: 5,
                      ),
                      Container(
                        margin: EdgeInsets.fromLTRB(0, 0, 20, 0),
                        child: Align(
                          alignment: Alignment.centerRight,
                          child: IconButton(
                            icon: Image.asset('assets/keranjang.png' ,width: 45, height: 45, ),  
                            alignment: Alignment.topLeft,
                            onPressed: () {
                              Navigator.pop(context);
                            },
                          ),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                      height: 10,
                  ),
                  Padding(
                    padding: EdgeInsets.fromLTRB(30, 0, 0, 0),
                    child: Align(
                      alignment: Alignment.centerLeft,
                      child:  Text(
                        'Hi, Ben',
                        textAlign: TextAlign.left,
                        style: GoogleFonts.overpass(
                          fontSize: 26,
                          fontWeight: FontWeight.w700,
                          color: Colors.white,
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.fromLTRB(30, 0, 0, 0),
                    child: Align(
                      alignment: Alignment.centerLeft,
                      child:  Text(
                        'Welcome to Medtech',
                        textAlign: TextAlign.left,
                        style: GoogleFonts.poppins(
                          fontSize: 16,
                          fontWeight: FontWeight.w300,
                          color: Colors.white),
                        ),
                      ),
                    ),
                  search(),
                  Padding(
                    padding: EdgeInsets.fromLTRB(30, 0, 0, 0),
                    child: Align(
                      alignment: Alignment.centerLeft,
                      child:  Text(
                        'Top Categories',
                        textAlign: TextAlign.left,
                        style: GoogleFonts.overpass(
                          fontSize: 18,
                          fontWeight: FontWeight.w700,
                          color: Color(0xFF090F47),
                        ),
                      ),
                    ),
                  ),
                  listkategori(),
                  
                ],
              ),
            ),
          ),  
        ],
      ),
    );
  }

  Widget background(){
    return Container(
      height: 250,
      decoration: BoxDecoration(
        color: Color(0xFF4157FF),
        borderRadius: BorderRadius.only(
          bottomLeft: Radius.circular(20),
          bottomRight: Radius.circular(20),
        ),
      ),
    );
  }
}



Widget search() {
    return Container(
      margin: EdgeInsets.fromLTRB(20, 30, 20, 20),
      height: 60,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(56),
      ),
      child: Row(
        children: [
          Expanded(
            child: TextField(
              decoration: InputDecoration(
                hintText: 'Search Medicine & Healthcare products',
                border: InputBorder.none,
                prefixIcon: Icon(
                  Icons.search,
                  color: Colors.indigo[900],
                ),
                isDense: true,
                contentPadding: EdgeInsets.all(0),
              ),
              textAlignVertical: TextAlignVertical.center,
            ),
          ),
        ],
      ),
    );
  }

Widget listkategori() {
    return SizedBox(
      height: 130,

      child: ListView.builder(
        itemCount: 8,  
        scrollDirection: Axis.horizontal,
        itemBuilder: (context, index) {
          return Container(
            height: 98,
            width: 64,
            margin: EdgeInsets.fromLTRB(
              index == 0 ? 16 : 8,
              0,
              index == Assets.profesors.length - 1 ? 16 : 8,
              0,
            ),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(70),
              color: Colors.white,
            ),
            child: ClipRRect(
              borderRadius: BorderRadius.circular(16),
              child: Image.asset(
                'assets/dental.png',
                fit: BoxFit.contain,
                height: 50,
                width: 50,
              ),
            ),
          );
        },
      ),
    );
  }
